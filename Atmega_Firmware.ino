/*
CamBuff Firmware based on Yanis Wireless EOS Controller

Includes code derivatives of Sandro Benigno(ArducamOSD) and Manishi Barosee (manis404)

(USB host and PTP library from Oleg Mazurov - circuitsathome.com)
(PTP 2.0 adaptation - Oleg Mazurov - circuitsathome.com 


This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

 This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along with this program. 
 If not, see <http://www.gnu.org/licenses/>.
 
*/

#include <avrpins.h>
#include <max3421e.h>
#include <usbhost.h>
#include <usb_ch9.h>
#include <Usb.h>
#include <usbhub.h>
#include <address.h>

#include <message.h>
#include <parsetools.h>

#include <ptp.h>
#include <canoneos.h>
#include "ptpobjhandleparser.h"


uint16_t x;
uint32_t temp;

class CamStateHandlers:public EOSStateHandlers
{

  bool stateConnected;

public:
  CamStateHandlers ():stateConnected (false) {};

  virtual void OnDeviceDisconnectedState (PTP * ptp);
  virtual void OnDeviceInitializedState (PTP * ptp);
} CamStates;

USB Usb;
CanonEOS Eos (&Usb, &CamStates);
PTPRawParser prs;
PTPObjHandleParser objprs;

void
CamStateHandlers::OnDeviceDisconnectedState (PTP * ptp)
{

  if (stateConnected)
    {
      stateConnected = false;
      Notify (PSTR ("Camera disconnected\r\n"));
    }
}

void
CamStateHandlers::OnDeviceInitializedState (PTP * ptp)
{
  if (!stateConnected)
    {
      stateConnected = true;
    }

  while (stateConnected == true)
    {
      readSerialCommand ();
    }
  delay (5000);
}

void
setup ()
{
  Serial.begin (57600);

  if (Usb.Init () == -1)
    {
      Serial.println ("OSC did not start.");
    }

  delay (200);
}

void
loop ()
{
  Usb.Task ();
}

void
readSerialCommand ()
{
  char queryType;
  if (Serial.available ())
    {
      queryType = Serial.read ();
      switch (queryType)
	{
	case 'C':		//Capture!!!
	  Eos.Capture ();
	  delay (500);
	  break;

	case 'X':		//Switch On Bulb
	  Eos.StartBulb ();
	  break;

	case 'Y':		//Switch Off Bulb
	  Eos.StopBulb ();
	  delay (500);
	  break;

	case 'V':		//Liveview ON/OFF
	  if ((uint16_t) readFloatSerial () == 0)
	    {
	      Eos.SwitchLiveView (false);
	    }
	  else
	    {
	      Eos.SwitchLiveView (true);
	    }
	  delay (500);
	  break;

	case 'I':		//Set ISO
	  Eos.SetProperty (EOS_DPC_Iso, (uint16_t) readFloatSerial ());
	  delay (1000);
	  break;

	case 'S':		//Set ShutterSpeed
	  Eos.SetProperty (EOS_DPC_ShutterSpeed,
			   (uint16_t) readFloatSerial ());
	  delay (500);
	  break;

	case 'W':		//Set Whitebalance
	  Eos.SetProperty (EOS_DPC_WhiteBalance,
			   (uint16_t) readFloatSerial ());
	  delay (500);
	  break;

	case 'A':		//Set Aperture
	  Eos.SetProperty (EOS_DPC_Aperture, (uint16_t) readFloatSerial ());
	  delay (500);
	  break;

	case 'F':		//MoveFocus
	  Eos.MoveFocus ((uint16_t) readFloatSerial ());
	  break;

	case 'E':		//Change Bracket Mode.
	  Eos.SetProperty (EOS_DPC_BracketMode,
			   (uint16_t) readFloatSerial ());
	  delay (500);
	  break;

	case 'T':		// Thumbnail Test
	  Eos.GetThumb (0x91905052, &prs);
	  break;

	case 'L':		// Get Thumbbail
	  Eos.GetObjectHandles (0xFFFFFFFF, 0, 0, &objprs);
	  Eos.GetThumb (objprs.GetLastAddress (), &prs);
	  break;
	}
    }
}


float
readFloatSerial ()
{
  byte index = 0;
  byte timeout = 0;
  char data[128] = "";

  do
    {
      if (Serial.available () == 0)
	{
	  delay (10);
	  timeout++;
	}
      else
	{
	  data[index] = Serial.read ();
	  timeout = 0;
	  index++;
	}
    }
  while ((data[constrain (index - 1, 0, 128)] != ';') && (timeout < 5)
	 && (index < 128));
  return atof (data);
}
